package com.respository.project;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.connection.database.DatabaseConnector;
import com.info.all.BlogInfo;
import com.info.all.CommentInfo;
import com.info.all.UserInfo;


public class RepositoryImplementation implements Repository {
	
	private DatabaseConnector connector;////////////////////////////
	String admin= "likhanbanik@gmail.com";

	public RepositoryImplementation() {
		
		 connector = new DatabaseConnector();
	}

	
    public void RegisterQuery(UserInfo user) {
	
	try{
		
		Connection conn = connector.getConnection();/////
		
		String query = "insert into registerdata (user_name,email,password,sex) "
				+ "values( '"
				+ user.getName()
				+ "' ,'"
				+ user.getEmail()
				+ "' , '"
				+ user.getPassword()
				+ "' , '"
				+ user.getSex()
				+ "' );";

		System.out.println(query);
		Statement statement = conn.createStatement();/////
		statement.execute(query);
		
		connector.closeConnection();
		}
	catch (SQLException e)
		{
			e.printStackTrace();
		}
    }
    
    public boolean LoginQuery(UserInfo user)
    {
    		boolean valid = false;
    		
    		try 
    		{
    			Connection conn = (Connection) connector.getConnection();
    			
    			String checkLoginQuery = "SELECT email,password FROM registerdata WHERE email ='"+ user.getEmail()+"'and password='"+user.getPassword()+"' ";
               
    			Statement statement = (Statement) conn.createStatement();
    			ResultSet result =statement.executeQuery(checkLoginQuery);
    			 
    			
    			if(result.next())		
    			{
    				String userMail= result.getString("email");
    				String userPassword=result.getString("password");
    				System.out.println(userMail);
    				System.out.println(userPassword);
    				if(userMail.equals(user.getEmail()) && userPassword.equals(user.getPassword()))
    				{
    					valid=true;
    				}			
    				else
    				{
    					valid=false;
    				}				
    				
    			}
    			connector.closeConnection();
    		}
    		
    		catch (SQLException e)
    		{
    			e.printStackTrace();
    		}
    		return valid;
    		
    	}


	@Override
	public boolean EmailValidation(String string) {
		
		boolean valid = false;
		
		try{
			
			Connection conn = connector.getConnection();/////
			
			String query = "select email from registerdata  where email ='"+ string+"'";
            
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(query);
			 
			
			if(result.next())		
			{
				String userMail= result.getString("email");
				System.out.println(userMail);
				
				if(userMail.equals(string))
				{
					System.out.println("email match "+string);
					valid=true;
				}			
				else
				{
					System.out.println("mail not match "+string);
					valid=false;
				}				
				
			}
			connector.closeConnection();
			
			}
		catch (SQLException e)
			{
				e.printStackTrace();
			}
		return valid;
	}




public void BlogWriteQuery(BlogInfo blog) {
	
	try{
		
		Connection conn = connector.getConnection();/////
		
		String query = "insert into blogdata (writer_email,TimeDate,headline,blogpost,blogtype) "
				+ "values( '"
				+ blog.getWriter_email()
				+ "' ,'"
				+ blog.getDate_time()
				+ "' , '"
				+ blog.getHeadline()
				+ "' ,'"
				+ blog.getMessage()
				+ "','"
				+ blog.getBlogType()
				+ "');";

		System.out.println(query);
		Statement statement = conn.createStatement();/////
		statement.execute(query);//////////
		
		connector.closeConnection();
		}
	catch (SQLException e)
		{
			e.printStackTrace();
		}
   }

public void setCommentData(CommentInfo comm) {
	
	try{
		
		Connection conn = connector.getConnection();/////
		
		String query = "insert into commentdata (blog_Id,TimeDate,commentor_email,comments) "
				+ "values( '"
				+ comm.getBlog_id()
				+ "' , '"
				+ comm.getDate_time()
				+ "' ,'"
				+ comm.getWriter_email()
				+ "','"
				+ comm.getComment()
				+ "');";

		System.out.println(query);
		Statement statement = conn.createStatement();/////
		statement.execute(query);//////////
		
		connector.closeConnection();
		}
	catch (SQLException e)
		{
			e.printStackTrace();
		}
   }

public ArrayList<BlogInfo> CheckWriteBlogQuery()
{	
   ArrayList<BlogInfo> bloglist = new ArrayList<BlogInfo>();
  // BlogInfo blogInfo = new BlogInfo();
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String checkLoginQuery = "SELECT TimeDate, headline, blogpost,writer_email,blogtype FROM blogdata ORDER BY id DESC LIMIT 1";
           
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =  statement.executeQuery(checkLoginQuery);
	
			while(result.next())
			{
				//emailInfo.setName(result.getString("name"));
				//emailInfo.setEmail_address(result.getString("email_address"));
				//emailInfo.setEmail_subject(result.getString("subject"));
				//emailInfo.setMessage(result.getString("message"));
				bloglist.add(new BlogInfo(result.getString("TimeDate"),result.getString("headline"),result.getString("blogpost"),result.getString("writer_email"),result.getString("blogtype")));
				
				
			}
			System.out.println("sizearray = "+bloglist.size());
			System.out.println("text = "+bloglist.get(0).getMessage());
			//System.out.println("sub = "+mailList.get(1).getEmail_subject());
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return bloglist;
	}



public ArrayList<BlogInfo> CheckShowQuery()
{	
   ArrayList<BlogInfo> bloglist = new ArrayList<BlogInfo>();
   BlogInfo blogInfo = new BlogInfo();
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String checkLoginQuery = "SELECT TimeDate, headline, blogpost,writer_email,blogtype  FROM blogdata ORDER BY id DESC LIMIT 100";
           
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =  statement.executeQuery(checkLoginQuery);
	
			while(result.next())
			{
				//emailInfo.setName(result.getString("name"));
				//emailInfo.setEmail_address(result.getString("email_address"));
				//emailInfo.setEmail_subject(result.getString("subject"));
				//emailInfo.setMessage(result.getString("message"));
				bloglist.add(new BlogInfo(result.getString("TimeDate"),result.getString("headline"),result.getString("blogpost"),result.getString("writer_email"),result.getString("blogtype")));
				
				
			}
			//System.out.println(bloglist.size());
			//System.out.println("sub = "+bloglist.get(0).getHeadline());
			//System.out.println("sub = "+mailList.get(1).getEmail_subject());
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return bloglist;
	}

public ArrayList<BlogInfo> ReadMoreQuery(String time)
{
	ArrayList<BlogInfo> bloglist = new ArrayList<BlogInfo>();
	   BlogInfo blogInfo = new BlogInfo();
		
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String checkLoginQuery = "SELECT writer_email,TimeDate,blogtype,headline,blogpost FROM blogdata WHERE TimeDate ='"+time+"'";
           
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(checkLoginQuery);
			 
			
			if(result.next())		
			{
				bloglist.add(new BlogInfo(result.getString("TimeDate"),result.getString("headline"),result.getString("blogpost"),result.getString("writer_email"),result.getString("blogtype")));
						
			}
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return bloglist;
	}


public ArrayList<BlogInfo> ReadTypeQuery(String time)
{
	ArrayList<BlogInfo> bloglist = new ArrayList<BlogInfo>();
	   BlogInfo blogInfo = new BlogInfo();
		
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String checkLoginQuery = "SELECT writer_email,TimeDate,blogtype,headline,blogpost FROM blogdata WHERE blogtype ='"+time+"'";
			System.out.println(checkLoginQuery);
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(checkLoginQuery);
			 
			
			while(result.next())		
			{
				bloglist.add(new BlogInfo(result.getString("TimeDate"),result.getString("headline"),result.getString("blogpost"),result.getString("writer_email"),result.getString("blogtype")));
						
			}
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return bloglist;
	}

public int getBlogid(String time)
{
	 int id = 0;
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String checkLoginQuery = "SELECT id FROM blogdata WHERE TimeDate ='"+time+"'";
			System.out.println(checkLoginQuery);
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =statement.executeQuery(checkLoginQuery);
		
			
			if(result.next())		
			{
				
				id = result.getInt("Id");		
			}
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return id;
	}

public ArrayList<CommentInfo> getCommentData(int id)
{	
   ArrayList<CommentInfo> commentList = new ArrayList<CommentInfo>();
  // BlogInfo blogInfo = new BlogInfo();
		try 
		{
			Connection conn = (Connection) connector.getConnection();
			
			String getCommentQuery = "SELECT timedate, commentor_email, comments FROM commentdata where blog_Id="+id+";";
            System.out.println(getCommentQuery);
			Statement statement = (Statement) conn.createStatement();
			ResultSet result =  statement.executeQuery(getCommentQuery);
	
			while(result.next())
			{
				//emailInfo.setName(result.getString("name"));
				//emailInfo.setEmail_address(result.getString("email_address"));
				//emailInfo.setEmail_subject(result.getString("subject"));
				//emailInfo.setMessage(result.getString("message"));
				commentList.add(new CommentInfo(id,result.getString("comments"),result.getString("timedate"),result.getString("commentor_email")));
				
				
			}
			//System.out.println("sizearra y = "+commentList.size());
			//System.out.println("text = "+commentList.get(0).getMessage());
			//System.out.println("sub = "+mailList.get(1).getEmail_subject());
			connector.closeConnection();
		}
		
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return commentList;
	}

}



