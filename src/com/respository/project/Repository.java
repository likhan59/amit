package com.respository.project;

import com.info.all.UserInfo;

public interface Repository {
	
	public void RegisterQuery(UserInfo user);
	
	public boolean LoginQuery(UserInfo user);
	
	public boolean EmailValidation(String string);

}
