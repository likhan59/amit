package com.connection.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.mysql.jdbc.*;

public class DatabaseConnector {

	String userName = "root";
	String password = "";
	// String databaseName = "user_info";
	String driver = "com.mysql.jdbc.Driver";
	String url = "jdbc:mysql://localhost:3306/projectdb";

	private Connection conn ;

	public Connection getConnection() {
		conn = null;
		try {
			Class.forName(driver);
			if (conn == null) {
				conn = DriverManager.getConnection(url, userName, password);
				System.out.println("Connected ...!!!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public void closeConnection() {
		try {
			conn.close();
			System.out.println("Disconnected ...!!!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}	
	

