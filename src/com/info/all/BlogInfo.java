package com.info.all;

public class BlogInfo {
	
	String headline;
	String message;
	String Writer_email;
	String Date_time;
	String blogType;
	
	
	public String getBlogType() {
		return blogType;
	}


	public void setBlogType(String blogType) {
		this.blogType = blogType;
	}


	public String getWriter_email() {
		return Writer_email;
	}


	public void setWriter_email(String writer_email) {
		Writer_email = writer_email;
	}


	public String getDate_time() {
		return Date_time;
	}


	public void setDate_time(String date_time) {
		Date_time = date_time;
	}
	
	
	public BlogInfo() {
		// TODO Auto-generated constructor stub
	}
	
	
	public BlogInfo(String time,String name,String blogpost,String email,String blogType) {
		// TODO Auto-generated constructor stub
		setDate_time(time);
		setHeadline(name);
		setMessage(blogpost);
		setBlogType(blogType);
		setWriter_email(email);
	}
	
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
