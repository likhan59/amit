package com.info.all;

public class CommentInfo {
	
	String Writer_email;
	String Date_time;
	String Comment;
	int Blog_id;
	public CommentInfo() {
		// TODO Auto-generated constructor stub
	}
	
	public CommentInfo(int blog_id,String comment,String date_time,String writer_email) {
		// TODO Auto-generated constructor stub
		setBlog_id(blog_id);
		setComment(comment);
		setDate_time(date_time);
		setWriter_email(writer_email);
		
	}
	
	public String getWriter_email() {
		return Writer_email;
	}
	public void setWriter_email(String writer_email) {
		Writer_email = writer_email;
	}
	public String getDate_time() {
		return Date_time;
	}
	public void setDate_time(String date_time) {
		Date_time = date_time;
	}
	public String getComment() {
		return Comment;
	}
	public void setComment(String comment) {
		Comment = comment;
	}
	public int getBlog_id() {
		return Blog_id;
	}
	public void setBlog_id(int blog_id) {
		Blog_id = blog_id;
	}
	

}
