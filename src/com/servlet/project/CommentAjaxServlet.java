package com.servlet.project;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.info.all.BlogInfo;
import com.info.all.CommentInfo;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class CommentAjaxServlet
 */
@WebServlet("/CommentAjaxServlet")
public class CommentAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		ArrayList<CommentInfo> blogList = new ArrayList<CommentInfo>();
		
		Date date = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String s = sdf.format(date);
		RepositoryImplementation getCommentData = new RepositoryImplementation();
		int id = getPostTime(request, getCommentData);
		String comment = request.getParameter("message");
		
		String writer_email=request.getParameter("email");
		
		CommentInfo coment = new CommentInfo();
		
		coment.setBlog_id(id);
		coment.setComment(comment);
		coment.setDate_time(s);
		coment.setWriter_email(writer_email);
		
		if(coment.getComment().length()!=0)
		{
			RepositoryImplementation insertintoDB = new RepositoryImplementation();
			insertintoDB. setCommentData(coment);
			blogList=insertintoDB.getCommentData(id);
		}
	}

	private int getPostTime(HttpServletRequest request,
			RepositoryImplementation getCommentData) {
		String time = request.getParameter("time");
		return getCommentData.getBlogid(time);
	}

}
