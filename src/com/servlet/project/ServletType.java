package com.servlet.project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.info.all.BlogInfo;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class ServletType
 */
@WebServlet("/ServletType")
public class ServletType extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletType() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
		String time = request.getParameter("type");
		System.out.println("typeooooooooooooooooooooooooooooooooooooooooooooooooooooooooo="+time);
		RepositoryImplementation getreadMoreData = new RepositoryImplementation();
		blogList = getreadMoreData.ReadTypeQuery(time);
		// System.out.println(blogList.get(0).getMessage());
		request.setAttribute("mailList", blogList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");  
		dispatcher.forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
//		String time = request.getParameter("c");
//		RepositoryImplementation getreadMoreData = new RepositoryImplementation();
//		blogList = getreadMoreData.ReadMoreQuery(time);
//		// System.out.println(blogList.get(0).getMessage());
//		request.setAttribute("mailList", blogList);
//		RequestDispatcher dispatcher = request.getRequestDispatcher("ReadBlog.jsp");  
//		dispatcher.forward(request, response); 
		
	}

}
