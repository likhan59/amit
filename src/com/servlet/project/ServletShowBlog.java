package com.servlet.project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.info.all.BlogInfo;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class ServletShowBlog
 */
@WebServlet("/ServletShowBlog")
public class ServletShowBlog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletShowBlog() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
		RepositoryImplementation insertintoDB = new RepositoryImplementation();
		blogList=insertintoDB.CheckShowQuery();
		//new CreateNewPage();
		request.setAttribute("mailList", blogList);
		RequestDispatcher dispatcher =request.getRequestDispatcher("index.jsp");  
				   
		dispatcher.forward(request, response);////////////////
//		try {
//		      if(dispatcher!=null){
//		        dispatcher.forward(request, response);
//		      }
//		    } catch (java.lang.NullPointerException e) {
//		        System.out.println("NullPointerException attribute expected in view");
//		    }
//		
	}
    
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		/*ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
		RepositoryImplementation insertintoDB = new RepositoryImplementation();
		blogList=insertintoDB.CheckShowQuery();
		//new CreateNewPage();
		request.setAttribute("mailList", blogList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");  
		dispatcher.forward(request, response);*/
		
	}

}
