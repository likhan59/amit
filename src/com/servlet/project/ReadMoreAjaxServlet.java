package com.servlet.project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.info.all.BlogInfo;
import com.info.all.CommentInfo;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class ReadMoreAjaxServlet
 */
@WebServlet("/ReadMoreAjaxServlet")
public class ReadMoreAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadMoreAjaxServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
		ArrayList<CommentInfo> commentList = new ArrayList<CommentInfo>();
		//System.out.println("time= "+request.getParameter("hidden"));
		String time = request.getParameter("hidden");
		System.out.println("time"+time);
		RepositoryImplementation getreadMoreData = new RepositoryImplementation();
		blogList = getreadMoreData.ReadMoreQuery(time);
		//String t = request.getParameter("time");
		// System.out.println(blogList.get(0).getMessage());
		int id = getreadMoreData.getBlogid(time);
		commentList = getreadMoreData.getCommentData(id);
		System.out.println("siz "+commentList.size());
		System.out.println("id "+id);
		request.setAttribute("mailList", blogList);
		request.setAttribute("commentList", commentList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("ReadBlog.jsp");  
		dispatcher.forward(request, response); 
	}

}
