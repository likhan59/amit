package com.servlet.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.SeekableByteChannel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.info.all.UserInfo;
import com.respository.project.RepositoryImplementation;



@WebServlet("/ServletLogin")

public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		PrintWriter out = response.getWriter();
		
		UserInfo user = new UserInfo();
		user.setEmail(request.getParameter("email"));
		user.setPassword(request.getParameter("password"));
		
		
			RepositoryImplementation log = new RepositoryImplementation();
			boolean s = log.LoginQuery(user);
			
			if(s)
			{
			HttpSession session= request.getSession();	
//			if(!session.isNew()) ////////// log out button press korle
//			{
////				session.invalidate();
//			}

			session.setAttribute("currentSessionUser", request.getParameter("email"));
			response.sendRedirect("showcall.jsp");
			
			}
			else
			{
				response.sendRedirect("LoginFailed.jsp");////////////banaite hoibo
			}
		
	}

}
