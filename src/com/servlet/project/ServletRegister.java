package com.servlet.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.SeekableByteChannel;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.connection.database.DatabaseConnector;
import com.info.all.UserInfo;
import com.respository.project.RepositoryImplementation;


/**
 * Servlet implementation class ServletRegister
 */
@SuppressWarnings("unused")
@WebServlet("/ServletRegister")
public class ServletRegister extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletRegister() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	
		String name,password,sex,email;
	
		
		UserInfo user = new UserInfo();
		PrintWriter print = response.getWriter();
		user.setName(request.getParameter("name"));
		user.setEmail(request.getParameter("email"));
		user.setSex( request.getParameter("sex"));
		if(request.getParameter("password").equals(request.getParameter("cpassword")))
		{
		user.setPassword( request.getParameter("password"));
		RepositoryImplementation insertintoDB = new RepositoryImplementation();
		insertintoDB.RegisterQuery(user);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("currentSessionUser", request.getParameter("email"));
		//response.sendRedirect("showcall.jsp");
		
		request.setAttribute("email", request.getParameter("email"));
		RequestDispatcher r = request.getRequestDispatcher("showcall.jsp");
        r.forward(request, response);
		}
		else
		{
			response.sendRedirect("Join.jsp");
		}
		
		
		
			
			/*request.setAttribute("mailList", mailList);
	        RequestDispatcher r = request.getRequestDispatcher("ContactMeList.jsp");
	        r.forward(request, response);*/
			
			
			//response.sendRedirect("index.jsp");
	}

}
