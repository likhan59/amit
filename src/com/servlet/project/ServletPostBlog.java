package com.servlet.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.info.all.BlogInfo;
import com.info.all.UserInfo;
import com.respository.project.RepositoryImplementation;

/**
 * Servlet implementation class ServletPostBlog
 */
@WebServlet("/ServletPostBlog")
public class ServletPostBlog extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPostBlog() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		ArrayList<BlogInfo> blogList = new ArrayList<BlogInfo>();
		Date date = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String s = sdf.format(date);
		//session.getatt
		BlogInfo blog = new BlogInfo();
		String headLine =request.getParameter("headline"); 
		headLine = headLine.replace("\"", "\\\"");
		System.out.println("hh ::"+headLine);
		blog.setWriter_email(request.getParameter("WriterEmail"));
		blog.setBlogType(request.getParameter("menuDropDown"));
		blog.setHeadline(headLine);
		blog.setMessage(request.getParameter("blog"));
		blog.setDate_time(s);
		
		System.out.println("date:"+s);
		//System.out.println(request.getParameter("menuDropDown"));
		
		if(blog.getHeadline().length()!=0 && blog.getMessage().length()!=0 )
		{
			RepositoryImplementation insertintoDB = new RepositoryImplementation();
			insertintoDB.BlogWriteQuery(blog);
			blogList=insertintoDB.CheckWriteBlogQuery();
			//new CreateNewPage();
			request.setAttribute("mailList", blogList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");///////make new page as readblog.jsp  
			dispatcher.forward(request, response);  
 
		}
		
	}

}
