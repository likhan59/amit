<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LoginFailed-Technology Blog</title>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700|Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<%@ include file="header.jsp" %>

<div>
		<div id="page">
			<div id="content_only">
				<div class="post">
				<%=session.invalidate() %>
						<h1>Login Failed, Please enter valid information </h1>
						<span><a id="redirectToLogin" href="Login.jsp">Login</a></span>			
					
					<!--  <p class="meta"><span class="date">April 05, 2013</span><span class="posted">Posted by <a href="#">Someone</a></span></p>-->
					<div style="clear: both;">&nbsp;</div>
					<div class="entry">
						
					</div>
				</div>


				<div style="clear: both;">&nbsp;</div>
			</div>
			<!-- end #content -->
			<div style="clear: both;">&nbsp;</div>
		</div>
   </div>
<%@ include file="Footer.jsp" %>

</div>

</body>
</html>