<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>header</title>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700|Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="wrapper">
	<div id="header-wrapper">
		<div id="header" class="container">
			<div id="logo">
				<h1><a href="#">Technology Blog</a></h1>
			</div>
			<div id="menu">
				<ul>
					<li class="current_page_item"><a href="showcall.jsp">Homepage</a></li>
					<li class="current_page_item"><a href="WriteBlog.jsp">Write Blog</a></li>
					<li class="current_page_item"><a href="AboutUs.jsp">About</a></li>
					<% 
					
					//System.out.println("header: "+session.getAttribute("currentSessionUser").toString());
					Object oj = session.getAttribute("currentSessionUser");
					if(oj != null)
					{
						
						%>
						
						<li class="current_page_item"><a href="SessionDestroy.jsp">Logout</a></li>
					<% 		
					}
					else{
						%>
						<li class="current_page_item"><a href="Login.jsp">Login</a></li>
					
					<% 	
					}
						%>
					
				</ul>
			</div>
			
		</div>
		
<%
Object obj1 = session.getAttribute("currentSessionUser");
if(obj1!=null)
{
%>
		<p id="loggedUserMail"><%=session.getAttribute("currentSessionUser")%></p>
				<%
}
%>	
		
		<div id="banner">
			<div class="content"><img src="images/img02.png" width="1000" height="300" alt="warning" /></div>
		</div>
	</div>
	<!-- end #header -->
</body>
</html>