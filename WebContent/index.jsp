<%@page import="java.nio.channels.SeekableByteChannel"%>
<%@page import="java.util.ArrayList" %>
<%@page import="com.info.all.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>HomePage-Technology Blog</title>
<link href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700|Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<%@ include file="header.jsp" %>
	<% 
			ArrayList<BlogInfo> newslist= new ArrayList<BlogInfo>();
	  		newslist=(ArrayList)request.getAttribute("mailList");
	  		int newscount= newslist.size();
	  		System.out.println("six: "+newscount);
	  		
		    if(!newslist.isEmpty())
	 		 {
	  			for(int i=0;i<newscount;i++)
			   {
		   %>
		  
		    <div id="page">
			<div id="content">
			<div class="post">
			
				<h2 class="title"><a href=""><%= newslist.get(i).getHeadline()%></a></h2>
				<p class="meta"><span class="date"><%= newslist.get(i).getDate_time()%></span><span class="posted">Posted by <a href="#"><%= newslist.get(i).getWriter_email() %></a></span></p>
				<div style="clear: both;">&nbsp;</div>
				<div class="entry" >
				<% 
				        String str2 = newslist.get(i).getMessage();
						if(str2.length()>100)str2 = str2.substring(0,100);
						String time = newslist.get(i).getDate_time();
						//String g= session.getAttribute("currentSessionUser");
						//System.out.println("session: "+g) ;
				 %>
					<p><% out.println("<pre  style='white-space: pre-wrap'>"+str2+"</pre>"); %></p>
					<!-- <p class="links"><button title="Read more of this blog" id="readMore"><a href="ReadBlog.jsp" target="_blank" class="more">Read More</a></button><button title="comment here" id="comments">Comments</button></p> -->
					
					<form action="ReadMoreAjaxServlet" method ="post" target="_blank">
						 <input type="hidden" value="<%=time%>" name="hidden"></input>
					     <input type="submit" title="Read more of this blog" id=<%=i%> value="ReadMore"/>
					     
					</form>
					<script type="text/javascript" src="JS/jQuery.js"></script>
					<script type="text/javascript" src="JS/external_script.js"></script>
				</div>
			</div>
			
			</div>
			<%
			  }
	  }
	%> 
			
			
		
		<!-- end #content -->
		<div id="sidebar">
			<ul>
				<li>
					<h2>Categories</h2>
					<ul>
						<li><a href="ServletType?type=asp.net" value="asp.net" name="type">ASP.NET</a></li>
						<li><a href="ServletType?type=c" value="c" name="type">C</a></li>
						<li><a href="ServletType?type=cpp" value="cpp" name="type">CPP</a></li>
						<li><a href="ServletType?type=cs" value="cs" name="type">C#</a></li>
						<li><a href="ServletType?type=css" value="css" name="type">CSS</a></li>
						<li><a href="ServletType?type=html" value="html" name="type">HTML</a></li>
						<li><a href="ServletType?type=java" value="java" name="type">Java</a></li>
						<li><a href="ServletType?type=javascript" value="javascript" name="type">JavaScript</a></li>
						<li><a href="ServletType?type=jquery" value="jquery" name="type">JQuery</a></li>
						<li><a href="ServletType?type=servletjsp" value="servletjsp" name="type">Jsp/Servlet</a></li>
					</ul>
				</li>
				
			</ul>
		</div>
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	
	
<%@ include file="Footer.jsp" %>
</body>
</html>
