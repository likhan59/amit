<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>WriteBlog-Technology Blog</title>
<link
	href='http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700|Open+Sans:400,300'
	rel='stylesheet' type='text/css'>
	<link href="style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<% 
    Object str = session.getAttribute("currentSessionUser");
	//System.out.println("emailValue= "+str);
	if(str != null)
	{
%>
	<%@ include file="header.jsp"%>

	<div id="page">
		<div id="content_only">
			<div class="post">
				<h2 class="title">
					<a href="#">Write Your Blog and Spread Your Knowledge</a>
				</h2>
				<div style="clear: both;">&nbsp;</div>
				<form action="ServletPostBlog" method="post">
				    <select class='dropdown' name='menuDropDown'>
				    <option value="none">Enter Your Blog On</option>
				    <option value="asp.net">ASP.net</option>
				    <option value="c">C</option>
				    <option value="cpp">C++</option>
				    <option value="c#">C#</option>
				    <option value="css">CSS</option>
				    <option value="html">HTML</option>
				    <option value="java">Java</option>
				    <option value="javascript">JavaScript</option>
				    <option value="jquery">jQuery</option>
				    <option value="servletjsp">JSP/Servlet</option>
				    
				    </select><br /><br/>
					<input type="text" class="design_headline" name="headline"
						placeholder="enter blog headline"> <br />
						<br /> <textarea name="blog" cols="100" rows="30"
							placeholder="enter your information">
</textarea> <br><br><input type="submit" value="Post"> 
								<input
									type="hidden" name="WriterEmail"
									value="<%=session.getAttribute("currentSessionUser").toString()%>" />
				</form>

			</div>
		</div>
		<div style="clear: both;">&nbsp;</div>
	</div>
	</div>
	<%@ include file="Footer.jsp"%>
	<% 
	}
	else
	{
		response.sendRedirect("Login.jsp");
	}
	%>
</body>
</html>
