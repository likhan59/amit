-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 05, 2013 at 11:03 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogdata`
--

CREATE TABLE IF NOT EXISTS `blogdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `writer_email` text NOT NULL,
  `TimeDate` varchar(45) NOT NULL,
  `blogtype` varchar(20) NOT NULL,
  `headline` text NOT NULL,
  `blogpost` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=136 ;

--
-- Dumping data for table `blogdata`
--

INSERT INTO `blogdata` (`id`, `writer_email`, `TimeDate`, `blogtype`, `headline`, `blogpost`) VALUES
(100, 'faysal.bit0313@gmail.com', '05/05/2013 23:34:58', 'java', 'Java - Overview', 'Java programming language was originally developed by Sun Microsystems, which was initiated by James Gosling and released in 1995 as core component of Sun Microsystems.s Java platform (Java 1.0 [J2SE]).\r\n\r\nAs of December 08 the latest release of the Java Standard Edition is 6 (J2SE). With the advancement of Java and its wide spread popularity, multiple configurations were built to suite various types of platforms. Ex: J2EE for Enterprise Applications, J2ME for Mobile Applications.\r\n\r\nSun Microsystems has renamed the new J2 versions as Java SE, Java EE and Java ME respectively. Java is guaranteed to be Write Once, Run Anywhere.'),
(101, 'faysal.bit0313@gmail.com', '05/05/2013 23:36:53', 'java', 'Java Environment Setup', 'Before we proceed further it is important that we set up the java environment correctly. This section guides you on how to download and set up Java on your machine. Please follow the following steps to set up the environment.\n\nJava SE is freely available from the link Download Java. So you download a version based on your operating system.'),
(102, 'faysal.bit0313@gmail.com', '05/05/2013 23:39:15', 'java', 'Java - Objects and Classes', 'Java is an Object Oriented Language. As a language that has the Object Oriented feature Java supports the following fundamental concepts:\r\n\r\nPolymorphism\r\nInheritance\r\nEncapsulation\r\nAbstraction\r\nClasses\r\nObjects\r\nInstance\r\nMethod\r\nMessage Parsing\r\nIn this chapter we will look into the concepts Classes and Objects.\r\n\r\nObject - Objects have states and behaviors. Example: A dog has states-color, name, breed as well as behaviors -wagging, barking, eating. An object is an instance of a class.\r\n\r\nClass - A class can be defined as a template/ blue print that describe the behaviors/states that object of its type support.'),
(103, 'faysal.bit0313@gmail.com', '05/05/2013 23:41:09', 'java', 'Java - String Class', 'Strings, which are widely used in Java programming, are a sequence of characters. In the Java programming language, strings are objects.\r\n\r\nThe Java platform provides the String class to create and manipulate strings.\r\n\r\nThe String class is immutable, so that once it is created a String object cannot be changed. If there is a necessity to make alot of modifications to Strings of characters then you should use String Buffer & String Builder Classes.'),
(106, 'likhan@gmail.com', '05/06/2013 00:28:42', 'css', 'What is CSS?', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\r\naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\r\n'),
(107, 'likhan@gmail.com', '05/06/2013 00:30:49', 'javascript', 'Intro to JS', 'Java script is a scripting language'),
(108, 'likhan@gmail.com', '05/06/2013 01:26:48', 'html', 'What is HTML?', 'HTML is not a programming language, but rather a markup language. If you already know XML, HTML will be a snap for you to learn. We urge you not to attempt to blow through this tutorial in one sitting. Instead, we recommend that you spend 15 minutes to an hour a day practicing HTML and then take a break to let the information settle in. We arent going anywhere!'),
(109, 'tanvir@gmail.com', '05/06/2013 01:56:21', 'html', 'HTML Element Syntax', 'An HTML element starts with a start tag / opening tag\r\nAn HTML element ends with an end tag / closing tag\r\nThe element content is everything between the start and the end tag\r\nSome HTML elements have empty content\r\nEmpty elements are closed in the start tag\r\nMost HTML elements can have attributes'),
(110, 'tanvir@gmail.com', '05/06/2013 01:57:29', 'html', 'Styling HTML with CSS', 'CSS was introduced together with HTML 4, to provide a better way to style HTML elements.\r\n\r\nCSS can be added to HTML in the following ways:\r\n\r\nInline - using the style attribute in HTML elements\r\nInternal - using the <style> element in the <head> section\r\nExternal - using an external CSS file\r\nThe preferred way to add CSS to HTML, is to put CSS syntax in separate CSS files.\r\n\r\nHowever, in this HTML tutorial we will introduce you to CSS using the style attribute. This is done to simplify the examples. It also makes it easier for you to edit the code and try it yourself.'),
(111, 'tanvir@gmail.com', '05/06/2013 01:58:37', 'css', 'What is CSS?', 'CSS stands for Cascading Style Sheets\r\nStyles define how to display HTML elements\r\nStyles were added to HTML 4.0 to solve a problem\r\nExternal Style Sheets can save a lot of work\r\nExternal Style Sheets are stored in CSS files'),
(112, 'tanvir@gmail.com', '05/06/2013 01:59:50', 'css', 'CSS background', 'CSS background properties are used to define the background effects of an element.\r\n\r\nCSS properties used for background effects:\r\n\r\nbackground-color\r\nbackground-image\r\nbackground-repeat\r\nbackground-attachment\r\nbackground-position'),
(113, 'tanvir@gmail.com', '05/06/2013 02:00:48', 'javascript', 'Intro to JS', 'JavaScript is THE scripting language of the Web.\r\n\r\nJavaScript is used in billions of Web pages to add functionality, validate forms, communicate with the server, and much more'),
(114, 'tanvir@gmail.com', '05/06/2013 02:01:26', 'javascript', 'JavaScript is a Scripting Language', 'A scripting language is a lightweight programming language.\r\n\r\nJavaScript is programming code that can be inserted into HTML pages.\r\n\r\nJavaScript inserted into HTML pages, can be executed by all modern web browsers.\r\n\r\nJavaScript is easy to learn.'),
(115, 'tanvir@gmail.com', '05/06/2013 02:02:26', 'jquery', 'What is jQuery?', 'jQuery is a lightweight, "write less, do more", JavaScript library.\r\n\r\nThe purpose of jQuery is to make it much easier to use JavaScript on your website.\r\n\r\njQuery takes a lot of common tasks that require many lines of JavaScript code to accomplish, and wraps them into methods that you can call with a single line of code.\r\n\r\njQuery also simplifies a lot of the complicated things from JavaScript, like AJAX calls and DOM manipulation.'),
(116, 'tanvir@gmail.com', '05/06/2013 02:02:50', 'jquery', 'jQuery features', 'HTML/DOM manipulation\r\nCSS manipulation\r\nHTML event methods\r\nEffects and animations\r\nAJAX\r\nUtilities'),
(117, 'tanvir@gmail.com', '05/06/2013 02:03:24', 'jquery', 'Why jQuery?', 'There are a lots of other JavaScript frameworks out there, but jQuery seems to be the most popular, and also the most extendable.'),
(118, 'tanvir@gmail.com', '05/06/2013 02:04:12', 'jquery', 'What is jQuery?', 'jQuery is a lightweight, "write less, do more", JavaScript library.\r\n\r\nThe purpose of jQuery is to make it much easier to use JavaScript on your website.\r\n\r\njQuery takes a lot of common tasks that require many lines of JavaScript code to accomplish, and wraps them into methods that you can call with a single line of code.\r\n\r\njQuery also simplifies a lot of the complicated things from JavaScript, like AJAX calls and DOM manipulation.\r\n\r\n'),
(119, 'tanvir@gmail.com', '05/06/2013 02:04:33', 'jquery', 'jQuery features', 'The jQuery library contains the following features:\r\n\r\nHTML/DOM manipulation\r\nCSS manipulation\r\nHTML event methods\r\nEffects and animations\r\nAJAX\r\nUtilities'),
(120, 'tanvir@gmail.com', '05/06/2013 02:05:27', 'asp.net', 'ASP.NET  intro', 'ASP.NET is a development framework for building web pages and web sites with HTML, CSS, JavaScript and server scripting.\r\n\r\nASP.NET supports three different development models:\r\nWeb Pages, MVC (Model View Controller), and Web Forms:'),
(121, 'tanvir@gmail.com', '05/06/2013 02:06:28', 'asp.net', 'What is Web Pages?', 'Web Pages is one of the 3 programming models for creating ASP.NET web sites and web applications.\r\n\r\nThe other two programming models are Web Forms and MVC (Model, View, Controller).\r\n\r\n'),
(122, 'tanvir@gmail.com', '05/06/2013 02:06:42', 'asp.net', 'Why asp.net', 'Web Pages is the simplest programming model for developing ASP.NET web pages. It provides an easy way to combine HTML, CSS, JavaScript and server code:\r\n\r\nEasy to learn, understand, and use\r\nBuilt around single web pages\r\nSimilar to PHP and Classic ASP\r\nServer scripting with Visual Basic or C#\r\nFull HTML, CSS, and JavaScript control\r\nWeb Pages are easy extendable with programmable Web Helpers, including database, video, graphics, social networking and much more.'),
(123, 'tanvir@gmail.com', '05/06/2013 02:08:24', 'servletjsp', 'What is servlet ?', 'A servlet is a Java programming language class used to extend the capabilities of servers that host applications accessed by means of a request-response programming model. Although servlets can respond to any type of request, they are commonly used to extend the applications hosted by web servers. For such applications, Java Servlet technology defines HTTP-specific servlet classes.'),
(124, 'tanvir@gmail.com', '05/06/2013 02:09:06', 'servletjsp', 'Servlet Lifecycle', 'The lifecycle of a servlet is controlled by the container in which the servlet has been deployed. When a request is mapped to a servlet, the container performs the following steps.\r\n\r\nIf an instance of the servlet does not exist, the web container\r\n\r\nLoads the servlet class.\r\n\r\nCreates an instance of the servlet class.\r\n\r\nInitializes the servlet instance by calling the init method. Initialization is covered in Creating and Initializing a Servlet.\r\n\r\nInvokes the service method, passing request and response objects. Service methods are discussed in Writing Service Methods.'),
(125, 'tanvir@gmail.com', '05/06/2013 02:10:00', 'servletjsp', 'Controlling Concurrent Access to Shared Resources', 'In a multithreaded server, shared resources can be accessed concurrently. In addition to scope object attributes, shared resources include in-memory data, such as instance or class variables, and external objects, such as files, database connections, and network connections.\r\n\r\nConcurrent access can arise in several situations:\r\n\r\nMultiple web components accessing objects stored in the web context.\r\n\r\nMultiple web components accessing objects stored in a session.'),
(126, 'tanvir@gmail.com', '05/06/2013 02:11:00', 'cs', 'C# Tutorial', 'C# is a simple, modern, general-purpose, object-oriented programming language developed by Microsoft within its .NET initiative led by Anders Hejlsberg.\r\n\r\nThis tutorial will teach you basic C# programming and will also take you through various advance concepts related to C# programming language.'),
(127, 'tanvir@gmail.com', '05/06/2013 02:11:40', 'cs', 'What is C#??', 'C# is a modern, general-purpose object oriented programming language developed by Microsoft and approved by Ecma and ISO.\r\n\r\nC# was developed by Anders Hejlsberg and his team during the development of .Net Framework.\r\n\r\nC# is designed for Common Language Infrastructure (CLI), which consists of the executable code and runtime environment that allows use of various high-level languages to be used on different computer platforms and architectures.'),
(128, 'tanvir@gmail.com', '05/06/2013 02:12:22', 'cs', 'Why c#  ???', 'The following reasons make C# a widely used professional language:\r\n\r\nModern, general purpose programming language\r\n\r\nObject oriented.\r\n\r\nComponent oriented.\r\n\r\nEasy to learn.\r\n\r\nStructured language.\r\n\r\nIt produces efficient programs.\r\n\r\nIt can be compiled on a variety of computer platforms.\r\n\r\nPart of .Net Framework'),
(129, 'tanvir@gmail.com', '05/06/2013 02:14:04', 'cs', 'What is C#', 'C# is a modern, general-purpose object oriented programming language developed by Microsoft and approved by Ecma and ISO.\r\n\r\nC# was developed by Anders Hejlsberg and his team during the development of .Net Framework.\r\n\r\nC# is designed for Common Language Infrastructure (CLI), which consists of the executable code and runtime environment that allows use of various high-level languages to be used on different computer platforms and architectures.'),
(130, 'tanvir@gmail.com', '05/06/2013 02:15:17', 'cs', 'Why C#', 'The following reasons make C# a widely used professional language:\r\n\r\nModern, general purpose programming language\r\n\r\nObject oriented.\r\n\r\nComponent oriented.\r\n\r\nEasy to learn.\r\n\r\nStructured language.\r\n\r\nIt produces efficient programs.\r\n\r\nIt can be compiled on a variety of computer platforms.\r\n\r\nPart of .Net Framework.'),
(131, 'tanvir@gmail.com', '05/06/2013 02:16:53', 'cpp', 'About C++', 'C++ (pronounced see plus plus) was developed by Bjarne Stroustrup at Bell Labs as an extension to C, starting in 1979. C++ was ratified in 1998 by the ISO committee, and again in 2003 (called C++03, which is what this tutorial will be teaching). A new version of the standard, known as C++11 has been made available since the time these tutorials were written â?? updates to the tutorial to cover C++11â?²s additions will be made in the appendix.'),
(132, 'tanvir@gmail.com', '05/06/2013 02:18:19', 'cpp', 'Before start with c++', 'This is the â??howâ?? step, where you determine how you are going to solve the problem you came up with in step 1. It is also the step that is most neglected in software development. The crux of the issue is that there are many ways to solve a problem â?? however, some of these solutions are good and some of them are bad. Too often, a programmer will get an idea, sit down, and immediately start coding a solution. This almost always generates a solution that falls into the bad category.'),
(133, 'tanvir@gmail.com', '05/06/2013 02:19:38', 'cpp', 'About C++ (part-2)', 'Typically, good solutions have the following characteristics:\r\n* They are straightforward\r\n* They are well documented\r\n* They can be easily extended (to add new features that were not originally anticipated)\r\n* They are modularized'),
(134, 'likhan@gmail.com', '05/06/2013 02:36:45', 'c', 'What is C?', 'C language tricky good pointers questions answers and explanation operators data types arrays structures questions \r\nfunctions recursion preprocessors, looping, file handling, strings questions switch case if else printf advance c linux objective types mcq faq interview questions and answers with explanation and solution for freshers or beginners. Placement online written test prime numbers Armstrong Fibonacci series factorial palindrome code programs examples on c c++ tutorials and pdf.\r\n'),
(135, 'likhan@gmail.com', '05/06/2013 02:38:41', 'c', 'strncmp()', 'This function compare n character in two strings to find out whether they are same or different.\r\n\r\nThe two string are compared character by character until there is a mismatch or end of one of the strings is reached, whichever occurs first.\r\n\r\nIf the two strings are identical, strncmp() return zero.\r\nIf they are not, it returns the numeric difference between the ASCII values of the first non-matching pair of characters.');

-- --------------------------------------------------------

--
-- Table structure for table `commentdata`
--

CREATE TABLE IF NOT EXISTS `commentdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `timedate` varchar(45) NOT NULL,
  `commentor_email` text NOT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

-- --------------------------------------------------------

--
-- Table structure for table `registerdata`
--

CREATE TABLE IF NOT EXISTS `registerdata` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(32) NOT NULL,
  `sex` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `registerdata`
--

INSERT INTO `registerdata` (`user_id`, `user_name`, `email`, `password`, `sex`) VALUES
(58, 'likhan', 'likhan.banik@facebook.com', '17edaef1197f8faf2db18178b8531f36', 'Male'),
(59, 'Abdullah Al Faysal', 'faysal.bit0313@gmail.com', '202cb962ac59075b964b07152d234b70', 'Male'),
(60, 'admin', 'admin@gmail.com', 'eb62f6b9306db575c2d596b1279627a4', 'Male'),
(61, 'likhan', 'likhan@gmail.com', '202cb962ac59075b964b07152d234b70', 'Male'),
(62, 'Ta9vir', 'tanvir@gmail.com', '202cb962ac59075b964b07152d234b70', 'Male');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
